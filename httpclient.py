#!/usr/bin/env python

import os
from requests import Session
import logging


class HttpClientResponse(object):
    def __init__(self, status_code=None, response=None, err_msg=None, success=None, body=None):
        self.success = success
        self.status_code = status_code
        self.response = response
        self.err_msg = err_msg
        self.body = body
    pass


class HttpClient(object):
    def __init__(self, host, username=None, password=None, verify_ssl=False):
        self._session = Session()
        self._session.auth = username, password
        self._session.headers.update({'Content-Type': 'application/json'})
        self._session.verify = verify_ssl
        self.__url = (host if host.startswith('http') else "https://" + host.lstrip('/')).rstip('/')

    def set_header(self, key, value=None):
        if value is None:
            self._session.headers.pop(key)
            logging.debug("Removed header (if existed): {}={}".format(key, value))
        else:
            self._session.headers.update({key: value})
            logging.debug("Added header: {}={}".format(key, value))


    def _make_call(self, path, **kwargs):
        request_params_all = [ "method", "params", "data", "cookies", "files", "auth", "timeout",
                               "allow_redirects", "proxies", "verify", "stream", "cert" ]
        request_params = { k: v for k, v in kwargs.items() if k in request_params_all }
        request_params['method'] = request_params.get('method', 'GET')
        success_codes = kwargs.get('success_codes', [200,])
        reponse_body_format = kwargs.get('response_body_format', 'json')
        url = '{}/{}'.format(self.__url, path.lstrip('/'))
        respose = HttpClientResponse()
        try:
            response.response = self._session.request(url=url, **requst_params)
            response.success = response.response.status_code in success_codes
            response.status_code = response.response.status_code
            response.body = response.response.json() if (response.success and response_body_format == 'json') else response.response.content
            logging.debug("Requeset {} {} succeeded with code {}".format(request_params['method'], url, request.status_code))
        except Exception as exc:
            logging.debug("Requeset {} {} failed with code {}: {}".format(request_params['method'], url, request.status_code, exc))
            response.success = False
            response.err_msg = exc
        return response

